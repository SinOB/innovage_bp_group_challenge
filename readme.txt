=== Innovage BuddyPress Group Challenge ===
Contributors: SinOB
Tags: buddypress
Requires at least: WP 3.9, BuddyPress 2.0.1
Tested up to: WP 4.1, BuddyPress 2.0.1
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Extends BuddyPress groups to have a challenge route. Allows administrator(s) 
to create and modify routes.

== Description ==

Innovage BuddyPress Group Challenge extends the BuddyPress Group Manage->Detail
page to have three new fields related to creating a challenge. 
* A challenge route: Routes as created using the plugin innovage_routes
* A challenge type: 
        a)Competitive dyad challenge 
        b)Collaborative dyad challenge
        c)Competitive individual challenge 
        d)Collaborative individual challenge
* A challenge end date: Date field

Requires innovage_routes plugin to be installed and to have routes created.

== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.

