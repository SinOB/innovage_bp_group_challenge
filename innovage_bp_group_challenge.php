<?php
/*
  Plugin Name: Innovage BuddyPress Group Challenge
  Plugin URI: https://bitbucket.org/SinOB/innovage_bp_group_challenge
  Description: Allows Buddypress Groups to choose a challenge route and set a
  challenge end date. Requires innovage_pedometer and innovage_routes plugins.
  Author: Sinead O'Brien
  Version: 1.0
  Author URI: https://bitbucket.org/SinOB
  Requires at least: WP 3.9, BuddyPress 2.0.1, Innovage_Pedometer, Innovage_Routes
  Tested up to: WP 4.1, BuddyPress 2.0.1
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

function innovage_bp_group_meta_init() {

    /** /
     * Get current group id and load meta_key value if passed. If not pass it 
     * blank @return string Text contained in group meta
     * @global type $wp
     * @param type $meta_key
     * @return type
     */
    function custom_field($meta_key = '') {
        global $wp;
        // If we are on the create page do not display this field
        if (strpos(home_url($wp->request), '/create/') == false) {
            return groups_get_groupmeta(bp_get_group_id(), $meta_key);
        }
    }

    /** /
     * Get the group challenge type e.g competitive dyad, competitive individual...
     * @return string
     */
    function get_group_challenge_type() {
        $challenge_approach = custom_field('challenge-approach');
        if (isset($challenge_approach) && $challenge_approach != '') {
            $types = innovage_bp_get_challenge_types();
            return $types[$challenge_approach];
        }
    }

    /** /
     * This function is our custom field's form that is called 
     * in create a group and when editing group details
     */
    function group_header_fields_markup() {
        $challenge_routes = innovage_routes_get_routes();
        $challenge_types = innovage_bp_get_challenge_types();
        ?>
        <label for="">Challenge Route</label>
        <select id="challenge-type" name="challenge-type">
            <option value="">&nbsp;</option>
            <?php foreach ($challenge_routes as $route) { ?>
                <option value="<?php echo $route->id; ?>"
                <?php
                if (custom_field('challenge-type') == $route->id) {
                    echo ' selected="selected"';
                }
                ?>>
                            <?php echo $route->title; ?>
                </option>
            <?php } ?>
        </select>
        <br/>
        <label for="">Challenge Type</label>
        <select id="challenge-approach" name="challenge-approach">
            <option value="">&nbsp;</option>
            <?php foreach ($challenge_types as $key => $value) { ?>
                <option value="<?php echo $key; ?>"
                <?php
                if (custom_field('challenge-approach') == $key) {
                    echo ' selected="selected"';
                }
                ?>>
                            <?php echo $value; ?>
                </option>
            <?php } ?>
        </select>
        <br/>
        <label for="challenge-end-date">Challenge End Date</label><br/>
        <input id="challenge-end-date" type="text" name="challenge-end-date" class="custom_date" value="<?php echo custom_field('challenge-end-date'); ?>" />    

        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('.custom_date').datepicker({
                    dateFormat: 'yy-mm-dd',
                    showOn: "both",
                    buttonImageOnly: true,
                    buttonImage: "<?php echo plugins_url('images/calendar_icon.jpg', __FILE__) ?>"
                });
            });
        </script>

        <?php
    }

    /** /
     * Saves the custom group meta – props to Boone for the functions
     *
     * @param type $group_id
     */
    function group_header_fields_save($group_id) {
        $plain_fields = array(
            'challenge-type',
            'challenge-end-date',
            'challenge-approach'
        );
        foreach ($plain_fields as $field) {
            $key = $field;
            if (isset($_POST[$key])) {
                $value = $_POST[$key];
                groups_update_groupmeta($group_id, $field, $value);
            }
        }

        // if save group as individual challenge make sure we 
        // dont have any partnerships i.e. delete if exist
        if ($_POST['challenge-approach'] === '3' || $_POST['challenge-approach'] === '4') {
            if (function_exists('innovage_pedometer_delete_group_partnerships')) {
                innovage_pedometer_delete_group_partnerships($group_id);
            }
        }
    }

    /** /
     * Show the custom challenge field in the group header
     */
    function show_field_in_header() {
        $route_id = custom_field('challenge-type');
        $route = innovage_routes_get_route($route_id);
        echo "<p> The challenge is: " . $route->title . ' (' . get_group_challenge_type() . ")</p>";
        echo "<p> The challenge ends: " . custom_field('challenge-end-date');
        $goal = $route->goal_amount;
        if (isset($goal) && !empty($goal) && $goal > 0) {
            echo "<br/>The challenge goal is " . $goal . " steps";
        }
        echo '</p>';
    }

    if (function_exists('bp_is_active')) {
        if (bp_is_active('groups')) {
            add_filter('groups_custom_group_fields_editable', 'group_header_fields_markup');
            add_action('groups_group_details_edited', 'group_header_fields_save');
            add_action('groups_created_group', 'group_header_fields_save');
            add_action('bp_group_header_meta', 'show_field_in_header');
            add_action('bp_group_header_meta_link', 'show_link');
        }
    }
}

/** /
 * Return the primary activity page for a challenge based on the challenge type
 * @param type $group_id
 * @return string
 */
function innovage_bp_group_challenge_get_header_link($group_id) {
    $challenge_type = groups_get_groupmeta($group_id, 'challenge-approach');
    $link = '';
    switch ($challenge_type) {
        case 1: //Competitive dyad challenge
            $link = 'partner_steps/';
            break;
        case 2: //Collaborative dyad challenge
            $link = 'combined_steps/';
            break;
        case 3: //Competitive individual challenge
            $link = 'steps_competitive/';
            break;
        case 4: //Collaborative individual challenge
            $link = 'steps_collaborative/';
            break;
        default:
            // do nothing
            break;
    }
    return $link;
}

/** /
 * Get the types of challenge available
 * @return type
 */
function innovage_bp_get_challenge_types() {
    return array(1 => 'Competitive dyad challenge',
        2 => 'Collaborative dyad challenge',
        3 => 'Competitive individual challenge',
        4 => 'Collaborative individual challenge'
    );
}

add_action('bp_include', 'innovage_bp_group_meta_init');

/** /
 * Include the plugin javascript and css scripts
 */
function innovage_group_enqueue_styles_scripts() {
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-css', plugins_url('css/jquery-ui.css', __FILE__));
}

add_action("wp_enqueue_scripts", "innovage_group_enqueue_styles_scripts");
?>
